#!/usr/bin/env bash
curl --user ${CIRCLE_TOKEN}: \
    --request POST \
    --form revision=90d2e30ccb77aa1e573a975110dc4986bfca7060\
    --form config=@config.yml \
    --form notify=false \
        https://circleci.com/api/v1.1/project/github/bogorya/budget-tracker/tree/circleci10