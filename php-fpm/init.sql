CREATE TABLE expense_category
(
    id integer NOT NULL,
    category_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT expense_category_pkey PRIMARY KEY (id)
);

ALTER TABLE expense_category
    OWNER to budget;
    
CREATE TABLE expense
(
    id integer NOT NULL,
    expense_category_id_id integer NOT NULL,
    expense_amount double precision NOT NULL,
    expense_stamp date NOT NULL,
    expense_notes character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    CONSTRAINT expense_pkey PRIMARY KEY (id),
    CONSTRAINT fk_2d3a8da6208758e0 FOREIGN KEY (expense_category_id_id)
        REFERENCES expense_category (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

ALTER TABLE expense
    OWNER to budget;

CREATE INDEX idx_2d3a8da6208758e0
    ON expense USING btree
    (expense_category_id_id)
    TABLESPACE pg_default;

CREATE TABLE income_category
(
    id integer NOT NULL,
    category_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT income_category_pkey PRIMARY KEY (id)
);

ALTER TABLE income_category
    OWNER to budget;

CREATE TABLE income
(
    id integer NOT NULL,
    income_category_id integer NOT NULL,
    income_amount double precision NOT NULL,
    income_stamp date NOT NULL,
    income_notes character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    CONSTRAINT income_pkey PRIMARY KEY (id),
    CONSTRAINT fk_3fa862d053f8702f FOREIGN KEY (income_category_id)
        REFERENCES income_category (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

ALTER TABLE income
    OWNER to budget;

CREATE INDEX idx_3fa862d053f8702f
    ON income USING btree
    (income_category_id)
    TABLESPACE pg_default;

CREATE TABLE limits
(
    id integer NOT NULL,
    limits_category integer NOT NULL,
    limits_date date NOT NULL,
    limits_amount double precision NOT NULL,
    limits_active boolean NOT NULL,
    CONSTRAINT limits_pkey PRIMARY KEY (id)
);

ALTER TABLE limits
    OWNER to budget;
    
CREATE TABLE options
(
    id integer NOT NULL,
    options_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    options_value integer NOT NULL,
    CONSTRAINT option_pkey PRIMARY KEY (id)
);

ALTER TABLE options
    OWNER to budget;
    
CREATE TABLE saving
(
    id integer NOT NULL,
    saving_amount double precision NOT NULL,
    saving_stamp date NOT NULL,
    saving_note character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    CONSTRAINT saving_pkey PRIMARY KEY (id)
);

ALTER TABLE saving
    OWNER to budget;
    
INSERT INTO options(id, options_name, options_value) VALUES(1, 'day_first', 1);

CREATE SEQUENCE expense_category_id_seq START 1;
ALTER SEQUENCE expense_category_id_seq
    OWNER TO budget;
CREATE SEQUENCE expense_id_seq START 1;
ALTER SEQUENCE expense_id_seq
    OWNER TO budget;
CREATE SEQUENCE income_category_id_seq START 1;
ALTER SEQUENCE income_category_id_seq
    OWNER TO budget;
CREATE SEQUENCE income_id_seq START 1;
ALTER SEQUENCE income_id_seq
    OWNER TO budget;
CREATE SEQUENCE limits_id_seq START 1;
ALTER SEQUENCE limits_id_seq
    OWNER TO budget;
CREATE SEQUENCE option_id_seq START 1;
ALTER SEQUENCE option_id_seq
    OWNER TO budget;
CREATE SEQUENCE saving_id_seq START 1;
ALTER SEQUENCE saving_id_seq
    OWNER TO budget;