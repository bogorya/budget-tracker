#!/bin/bash

set -e

#sleep 5
php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force
php bin/console doctrine:migrations:migrate --no-interaction --allow-no-migration

exec "$@"