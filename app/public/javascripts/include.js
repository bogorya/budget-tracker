$(document).ready(function() {
  $( ".stampdate" ).datepicker({
    dateFormat: "dd.mm.yy",
    firstDay: 1
  });
  $.datepicker.setDefaults($.datepicker.regional["pl"]);
  $(".przycisk").button();
  $(".deletebutton").button();
  $(".hide").button();
  $(".go").click(function() {
    var href = $(this).attr('link');
    location.href=href;
  });
  $(".hide").click(function(event) {
    event.preventDefault();
    var div = $(this).attr('part');
    var cid = $(this).attr('cid');
    var pref = $(this).attr('prefix');
    $("#"+div).hide("slow");
    if (cid.length) $("#"+cid).val("");
    if ($("input[name="+pref+"amount]").length) $("input[name="+pref+"amount]").val("");
    if ($("input[name="+pref+"stamp]").length) $("input[name="+pref+"stamp]").val($.datepicker.formatDate('dd.mm.yy', new Date()));
    if ($("input[name="+pref+"notes]").length) $("input[name="+pref+"notes]").val("");
  });
  $(".expform").click(function() {
    var div = $(this).attr('part');
    var cid = $(this).attr('cid');
    var cidv = $(this).attr('cidv');
    $("#"+div).show("slow");
    $("#"+cid).val(cidv);
    $.post('include/getecname.php',{
        c_id:cidv
    }, function(data) {	
        if (data.cname!='') { 
         $('#ecname').html(data.cname);
        }						
    },'json');
    $.post('include/getexpenses.php',{
        c_id:cidv
    }, function(data) {
        $('#expenses').html('');
        if (data.exps.length > 0) {
          var list = $('<ul/>').appendTo('#expenses');
          for (var i = 0; i < data.exps.length; i++) {
            var ndate = $.datepicker.formatDate('dd.mm.yy', new Date(data.exps[i].e_stamp));
            list.append('<li>'+ndate +' <b>' + data.exps[i].c_name + '</b>: <a href="index.php?action=expense&eid=' + data.exps[i].e_id + '">' + data.exps[i].e_amount + '&nbsp;zł</a>' + (data.exps[i].e_notes!=''?' (' + data.exps[i].e_notes +')':'') + '</li>');
          }
         //
        }						
    },'json');
    $('html, body').animate({
        scrollTop: $("#exp_form").offset().top
    }, 2000);
  });
  $(".incform").click(function() {
    var div = $(this).attr('part');
    var cid = $(this).attr('cid');
    var cidv = $(this).attr('cidv');
    $("#"+div).show("slow");
    $("#"+cid).val(cidv);
    $.post('include/geticname.php',{
        c_id:cidv
    }, function(data) {	
        if (data.cname!='') { 
         $('#icname').html(data.cname);
        }						
    },'json');
  });
  $(".deletebutton").click(function(event) {
    if (confirm('Czy na pewno chcesz to usunąć?')) {
      return true;
    } else return false;
  });
  $(".movel").click(function() {
    var amount = Number($('#m_amount').val());
    var m0 = Number($('#m_c_id0').val());
    var m1 = Number($('#m_c_id1').val());
    var limit = Number($('#m_c_id0 option:selected').attr('limit'));
    var finish = true;
    if (amount == 0) {
      alert('Wpisana kwota musi być większa od 0');
      finish = false;
    }
    if (m0 == m1) {
      alert('Nie możesz przesunąć kwoty do tej samej kategorii');
      finish = false;
    }
    if (amount > limit) {
      alert('Wpisana kwota jest większa niż dostępna w kategorii');
      finish = false;
    }
    return finish;
  });
});