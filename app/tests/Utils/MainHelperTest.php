<?php
// tests/Utils/MainHelperTest.php
namespace App\Tests\Utils;

use App\Utils\MainHelper;
use PHPUnit\Framework\TestCase;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


class MainHelperTest extends WebTestCase
{

    /**  
    * @var EntityManagerInterface  
    */  
    private $em;

    public function setUp()  
    {  
        parent::setUp();

        $client = static::createClient();

        $application = new Application($client->getKernel());
        $application->setAutoExit(false);
        $output = new BufferedOutput();
        
        $input = new ArrayInput(array(
            'command' => 'doctrine:database:drop --if-exists --force',
            '--no-interaction' => true,
            '--env' => 'TEST'
        ));
        $input = new ArrayInput(array(
            'command' => 'doctrine:database:create',
            '--no-interaction' => true,
            '--env' => 'TEST'
        ));
        $application->run($input, $output);
        $input = new ArrayInput(array(
            'command' => 'doctrine:schema:update --force',
            '--env' => 'TEST'
        ));
        $application->run($input, $output);
        $input = new ArrayInput(array(
            'command' => 'doctrine:migrations:migrate',
            '--env' => 'TEST'
        ));
        $application->run($input, $output);

        //$this->em = $this->createMock(EntityManagerInterface::class);
        $kernel = self::bootKernel();
        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }  
    
    /*public function testMoneyLeftInLimits()
    {
        $helper = new MainHelper($this->em);
        $date1 = mktime(0, 0, 0, date("m"), 1, date("Y"));
        $date2 = mktime(23, 59, 59, date("m"), date("t"), date("Y"));
        $result = $helper->moneyLeftInLimits([$date1, $date2]);
        $this->assertGreaterThanOrEqual(0, $result);
    }*/

    public function testCheckDB()
    {
        $helper = new MainHelper($this->em);
        $result = $helper->checkDB();
        $this->assertEquals($result, '');
    }

    public function testcurrentPeriod()
    {
        $helper = new MainHelper($this->em);
        $result = $helper->currentPeriod();
        $this->assertNotEmpty($result);
    }
}