<?php
// tests/Utils/ExpCatTest.php
namespace App\Tests\Utils;

use App\Controller\Api\ExpenseCategoryApiController;
use PHPUnit\Framework\TestCase;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class ExpCatTest extends WebTestCase
{

    /**  
    * @var EntityManagerInterface  
    */  
    private $em;

    private $dbem;

    private $client;

    public function setUp()  
    {  
        parent::setUp();

        $client = static::createClient();

        $application = new Application($client->getKernel());
        $application->setAutoExit(false);
        $output = new BufferedOutput();
        
        $input = new ArrayInput(array(
            'command' => 'doctrine:database:drop --if-exists --force',
            '--no-interaction' => true,
            '--env' => 'TEST'
        ));
        $input = new ArrayInput(array(
            'command' => 'doctrine:database:create',
            '--no-interaction' => true,
            '--env' => 'TEST'
        ));
        $application->run($input, $output);
        $input = new ArrayInput(array(
            'command' => 'doctrine:schema:update --force',
            '--env' => 'TEST'
        ));
        $application->run($input, $output);
        $input = new ArrayInput(array(
            'command' => 'doctrine:migrations:migrate',
            '--env' => 'TEST'
        ));
        $application->run($input, $output);

        //$this->em = $this->createMock(EntityManagerInterface::class);
        $kernel = self::bootKernel();
        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->dbem = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $this->client = $this->createClient();
    }  

    public function testInsertCategory()
    {
        $this->withRollback(function() {
            $this->client->request(
                'http://127.0.0.1:9000/api/addexpensecategory',
                'POST',
                array('name' => 'TestCatExp')
            );
            $this->assertEquals($this->client->getResponse()->getStatusCode(), Response::HTTP_OK, 'Should return HTTP Code 200');
        });
    }

    public function withRollback($function)
    {
        try {
            $this->dbem->beginTransaction();
            $res = $function();
        } finally {
            $this->dbem->rollback();
        }

        return $res;
    }

}