<?php
//src/Utils/ExpenseHelper.php
namespace App\Utils;

use App\Entity\Expense;
use Doctrine\ORM\EntityManagerInterface;

/**
 * ExpenseHelper
 *
 * Set of functions used by controllers
 * 
 * @category Class
 * @package  Budget-tracker
 * @author   "Piotr Buczkowski <piotr.buczkowski@espeo.eu>"
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://github.com/bogorya/budget-tracker
 */
class ExpenseHelper
{

    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Returns array of expenses
     * 
     * @return Array
     */
    public function getAllExpenses(): array
    {
        $repository = $this->manager->getRepository(Expense::class);
        $expenses = $repository->findAll();
        return $expenses;
    }

    /**
     * Returns sum of Expenses between dates, given in array [start, end]
     * 
     * @param Array $dates begin and end dates of checked period
     * 
     * @return Float
     */
    public function calculateExpenses(array $dates): ?float
    {
        $repository = $this->manager->getRepository(Expense::class);
        $Expenses = $repository->findSum($dates);
        return $Expenses;
    }

}