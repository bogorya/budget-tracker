<?php
//src/Utils/LimitsHelper.php
namespace App\Utils;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\ExpenseCategory;
use App\Entity\Expense;
use App\Entity\Limits;

/**
 * LimitsHelper
 *
 * Set of functions used by controllers regarding limits
 * 
 * @category Class
 * @package  Budget-tracker
 * @author   "Piotr Buczkowski <piotr.buczkowski@espeo.eu>"
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://github.com/bogorya/budget-tracker
 */
class LimitsHelper
{

    protected $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Returns sum of limits between dates, given in array [start, end]
     * 
     * @param Array $dates begin and end dates of checked period
     * 
     * @return Float
     */
    public function calculateLimits(array $dates): ?float
    {
        $repository = $this->manager->getRepository(Limits::class);
        $limits = $repository->findSum($dates);
        return $limits;
    }

    /**
     * Returns sum of unused limits in expense categories between dates, 
     * given in array [start, end]
     * 
     * @param Array $dates begin and end dates of checked period
     * 
     * @return Float
     */
    public function moneyLeftInLimits(array $dates): ?float
    {
        $left = 0;
        try {
            $repository = $this->manager->getRepository(ExpenseCategory::class);
            $ecats = $repository->findAll();
            if (count($ecats) > 0) {
                foreach ($ecats as $ecat) {
                    $repository1 = $this->manager->getRepository(Limits::class);
                    $limit = $repository1->getLimitValue($ecat->getId(), $dates);
                    $repository2 = $this->manager->getRepository(Expense::class);
                    $catExpenses = $repository2->findSum($dates, $ecat->getId());
                    if ($limit > 0 && ($limit - $catExpenses) < 0) {
                        $left += abs($limit-$wydatki);
                    }
                }
            }
            return $left;
        } catch (\Exception $e) {
            return 0;
        }
    }
}