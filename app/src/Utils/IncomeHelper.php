<?php
//src/Utils/IncomeHelper.php
namespace App\Utils;

use App\Entity\Income;
use Doctrine\ORM\EntityManagerInterface;

/**
 * IncomeHelper
 *
 * Set of functions used by controllers regarding incomes
 * 
 * @category Class
 * @package  Budget-tracker
 * @author   "Piotr Buczkowski <piotr.buczkowski@espeo.eu>"
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://github.com/bogorya/budget-tracker
 */
class IncomeHelper
{

    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Returns array of incomes
     * 
     * @return Array
     */
    public function getAllIncomes(): array
    {
        $repository = $this->manager->getRepository(Income::class);
        $incomes = $repository->findAll();
        return $incomes;
    }

    /**
     * Returns sum of incomes between dates, given in array [start, end]
     * 
     * @param Array $dates begin and end dates of checked period
     * 
     * @return Float|null
     */
    public function calculateIncomes(array $dates): ?float
    {
        $repository = $this->manager->getRepository(Income::class);
        $incomes = $repository->findSum($dates);
        return $incomes;
    }

}