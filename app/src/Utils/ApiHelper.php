<?php
//src/Utils/ApiHelper.php
namespace App\Utils;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * ApiHelper
 *
 * Set of functions used by controllers
 * 
 * @category Class
 * @package  Budget-tracker
 * @author   "Piotr Buczkowski <piotr.buczkowski@espeo.eu>"
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://github.com/bogorya/budget-tracker
 */
class ApiHelper
{

    function getContent(object $em) {
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);
        return $serializer->serialize($em, 'json');
    }
}