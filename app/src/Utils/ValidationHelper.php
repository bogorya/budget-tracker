<?php
//src/Utils/ValidationHelper.php
namespace App\Utils;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
/**
 * ValidationHelper
 *
 * Set of functions used by controllers
 * 
 * @category Class
 * @package  Budget-tracker
 * @author   "Piotr Buczkowski <piotr.buczkowski@espeo.eu>"
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://github.com/bogorya/budget-tracker
 */
class ValidationHelper
{

    public function validateNameInput(Request $request): JsonResponse
    {
        $validator = Validation::createValidator();
        $violations = $validator->validate($request->get('name'), array(
            new Length(array('min' => 3)),
            new NotBlank(),
        ));
        if (0 !== count($violations)) {
            $errors = [];
            foreach ($violations as $violation) {
                $errors[] = $violation->getMessage();
            }
            return new JsonResponse(array('status' => Response::HTTP_BAD_REQUEST, 'data' => $errors));
        } else {
            return new JsonResponse(array('status' => Response::HTTP_OK, 'data' => []));
        }
    }

    public function validateAmountInput(Request $request): JsonResponse
    {
        $validator = Validation::createValidator();
        $violations = $validator->validate($request->get('amount'), array(
            new Type('numeric'),
            new NotBlank(),
            new GreaterThan(0)
        ));
        if (0 !== count($violations)) {
            $errors = [];
            foreach ($violations as $violation) {
                $errors[] = $violation->getMessage();
            }
            return new JsonResponse(array('status' => Response::HTTP_BAD_REQUEST, 'data' => $errors));
        } else {
            return new JsonResponse(array('status' => Response::HTTP_OK, 'data' => []));
        }
    }
}