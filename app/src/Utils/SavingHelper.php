<?php
//src/Utils/SavingHelper.php
namespace App\Utils;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Saving;


/**
 * SavingHelper
 *
 * Set of functions used by controllers
 * 
 * @category Class
 * @package  Budget-tracker
 * @author   "Piotr Buczkowski <piotr.buczkowski@espeo.eu>"
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://github.com/bogorya/budget-tracker
 */
class SavingHelper
{

    protected $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Returns array of savings
     * 
     * @return Array
     */
    public function getAllSavings(): array
    {
        $repository = $this->manager->getRepository(Saving::class);
        $saves = $repository->findAll();
        return $saves;
    }

    /**
     * Returns sum of savings between dates, given in array [start, end]
     * 
     * @param Array $dates begin and end dates of checked period
     * 
     * @return Float
     */    
    public function calculateSavings(array $dates): ?float
    {
        $repository = $this->manager->getRepository(Saving::class);
        $savings = $repository->findSum($dates);
        return $savings;
    }
}