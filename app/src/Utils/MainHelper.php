<?php
//src/Utils/MainHelper.php
namespace App\Utils;

use App\Entity\Options;
use Doctrine\ORM\EntityManagerInterface;
use App\Utils\IncomeHelper;
use App\Utils\SavingHelper;
use App\Utils\LimitsHelper;

/**
 * MainHelper
 *
 * Set of functions used by controllers
 * 
 * @category Class
 * @package  Budget-tracker
 * @author   "Piotr Buczkowski <piotr.buczkowski@espeo.eu>"
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://github.com/bogorya/budget-tracker
 */
class MainHelper
{

    protected $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Calculating actual period (1 month) taking the option 'day_first'
     * from database, and returning begin and end date of the period
     * If $timestamp is true, returns array of timestamps,
     * otherwise returns array of dates
     * 
     * @param Boolean $timestamps true/false determining of return values
     * 
     * @return Array
     */
    public function currentPeriod(int $timestamps = 0): array 
    {
        date_default_timezone_set("Europe/Warsaw");
        $repository = $this->manager->getRepository(Options::class);
        $option = $repository->findOneByName('day_first');
        $dayfirst = $option->getOptionValue();
        $actualday = date("d");
        if ($actualday >= $dayfirst) {
            $begin = mktime(0, 0, 0, date("m"), $dayfirst, date("Y"));
            $end = mktime(0, 0, 0, date("m")+1, $dayfirst-1, date("Y"));
        } else {
            $begin = mktime(0, 0, 0, date("m")-1, $dayfirst, date("Y"));
            $end = mktime(0, 0, 0, date("m"), $dayfirst-1, date("Y"));
        }
        switch ($timestamps) {
            case 1:
                return [date("d.m.Y", $begin), date("d.m.Y", $end)];
                break;
            case 2:
                return [date("Y-m-d", $begin), date("Y-m-d", $end)];
                break;
            default:
                return [$begin, $end];
                break;
        }
    }

    public function checkDB(): String
    {
        $repository = $this->manager->getRepository(Options::class);
        try {
            $option = $repository->findOneByName('day_first');
            return '';
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Returns array of calculated amounts for starting page between dates, 
     * given in array [start, end]
     * 
     * @param Array $dates begin and end dates of checked period
     * 
     * @return Array
     */
    public function mainInfo(
        IncomeHelper $ihelper, 
        SavingHelper $shelper, 
        LimitsHelper $lhelper, 
        ExpenseHelper $ehelper, 
        array $dates = []): array
    {
        if (empty($dates)) {
            $dates = $this->currentPeriod(2);
        
        }
        $totalIncomes = $ihelper->calculateIncomes($dates);
        $totalSavings = $shelper->calculateSavings($dates);
        $totalLimits = $lhelper->calculateLimits($dates);
        $totalExpenses = $ehelper->calculateExpenses($dates);
        $debet = $limitsLeft = 0; 
        if ($totalLimits > ($totalIncomes - $totalSavings)) {
            $debet = $totalLimits - ($totalIncomes - $totalSavings);
        } else {
            $limitsLeft = $totalIncomes - $totalSavings - $totalLimits;
        }
        $moneyLeftInLimits = $lhelper->moneyLeftInLimits($dates);
        $saldo = ($totalLimits > 0)?($totalLimits - $totalExpenses):($totalIncomes - $totalExpenses);
        return [
            'totalincomes' => $totalIncomes,
            'totalsavings' => $totalSavings,
            'totallimits' => $totalLimits,
            'totalexpenses' => $totalExpenses,
            'debet' => $debet,
            'limitsLeft' => $limitsLeft,
            'moneyleftinlimits' => $moneyLeftInLimits,
            'saldo' => $saldo
        ];
    }
}