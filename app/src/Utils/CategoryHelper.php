<?php
//src/Utils/CategoryHelper.php
namespace App\Utils;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\ExpenseCategory;
use App\Entity\IncomeCategory;

/**
 * CategoryHelper
 *
 * Set of functions used by controllers for categories
 * 
 * @category Class
 * @package  Budget-tracker
 * @author   "Piotr Buczkowski <piotr.buczkowski@espeo.eu>"
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://github.com/bogorya/budget-tracker
 */
class CategoryHelper
{

    protected $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Returns array of expense categories
     * 
     * @return Array
     */
    public function getAllECategories(): array
    {
        $repository = $this->manager->getRepository(ExpenseCategory::class);
        $ecats = $repository->findAll();
        return $ecats;
    }

    /**
     * Returns array of expense categories, with corresponding limits information
     * 
     * @return Array
     */
    public function getAllECategoriesWithLimits(\DateTime $date): array
    {
        $repository = $this->manager->getRepository(ExpenseCategory::class);
        $ecats = $repository->findAllWithLimits($date);
        return $ecats;
    }

    /**
     * Returns array of income categories
     * 
     * @return Array
     */
    public function getAllICategories(): array
    {
        $repository = $this->manager->getRepository(IncomeCategory::class);
        $icats = $repository->findAll();
        return $icats;
    }
}