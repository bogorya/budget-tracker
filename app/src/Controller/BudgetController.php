<?php
// src/Controller/BudgetController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Utils\MainHelper;
use App\Utils\IncomeHelper;
use App\Utils\SavingHelper;
use App\Utils\LimitsHelper;
use App\Utils\ExpenseHelper;

/**
 * BudgetController
 *
 * @category Class
 * @package  Budget-tracker
 * @author   "Piotr Buczkowski <piotr.buczkowski@espeo.eu>"
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://github.com/bogorya/budget-tracker
 */
class BudgetController extends Controller
{
    /**
     * @Route("/")
     * @Route("/homepage")
     * 
     * Main function of the application, 
     * displaying financial information from entered data
     * 
     * @param MainHelper $helper instance of MainHelper class
     * 
     * @return Twig
     */
    public function index(
        MainHelper $helper, 
        IncomeHelper $ihelper, 
        SavingHelper $shelper,
        LimitsHelper $lhelper,
        ExpenseHelper $ehelper
        )
    {
        $dbexist = $helper->checkDB();
        if ($dbexist == '') {
            return $this->render('budget/index.html.twig', 
                [
                'current_period' => $helper->currentPeriod(1),
                'main_info' => $helper->mainInfo($ihelper, $shelper, $lhelper, $ehelper)
                ]
            );
        } else {
            return $this->render('errordb2.html.twig', 
                [
                'exception' => $dbexist
                ]
            );
        }
    }

}