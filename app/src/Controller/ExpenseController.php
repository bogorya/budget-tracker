<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Utils\ExpenseHelper;
use App\Utils\MainHelper;
use App\Entity\Expense;
use Symfony\Component\HttpFoundation\Request;
use App\Forms\ExpenseType;

/**
 * ExpenseController
 *
 * @category Class
 * @package  Budget-tracker
 * @author   "Piotr Buczkowski <piotr.buczkowski@espeo.eu>"
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://github.com/bogorya/budget-tracker
 */
class ExpenseController extends Controller
{
    /**
     * @Route("/expense", name="expense")
     * 
     * Adding new expense
     * 
     * @param MainHelper $mainhelper instance of MainHelper class
     * @param Request $request request
     * 
     * @return Twig
     */
    public function index(ExpenseHelper $helper, MainHelper $mainhelper, Request $request)
    {
        $expense = new Expense();
        $form = $this->createForm(ExpenseType::class, $expense);
        $form->get('expenseStamp')->setData(new \DateTime);
        $form->add('save', SubmitType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $expense = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($expense);
            try {
                $entityManager->flush();
                return $this->redirectToRoute('expense');
            } catch (\Exception $e) {
                return $this->render('errordb.html.twig', [
                    'current_period' => $mainhelper->currentPeriod(1),
                    'exception' => $e->getMessage()
                ]);
            }
        }
        try {
            $expenses = $helper->getAllExpenses();
            return $this->render('expense/index.html.twig', 
                [
                'current_period' => $mainhelper->currentPeriod(1),
                'form' => $form->createView(),
                'expenses' => $expenses
                ]
            );
        } catch (\Exception $e) {
            return $this->render('errordb2.html.twig', [
                'current_period' => $mainhelper->currentPeriod(1),
                'exception' => $e->getMessage()
            ]);
        }        
    }

    /**
     * @Route("/expense/{id}", name="expense_edit")
     * 
     * Updating expense
     * 
     * @param int $id ID of the expense
     * @param MainHelper $mainhelper instance of MainHelper class
     * @param Request $request request
     * 
     * @return Twig
     */
    public function edit(int $id, MainHelper $mainhelper, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $expense = $entityManager->getRepository('App:Expense')->find($id);
        $form = $this->createForm(ExpenseType::class, $expense);
        $form->add('update', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $entityManager->flush();
                return $this->redirectToRoute('expense');
            } catch (\Exception $e) {
                return $this->render('errordb.html.twig', [
                    'current_period' => $mainhelper->currentPeriod(1),
                    'exception' => $e->getMessage()
                ]);
            }
        }
        return $this->render('expense/edit.html.twig', 
            [
            'current_period' => $mainhelper->currentPeriod(1),
            'form' => $form->createView()
            ]
        );
    }
}
