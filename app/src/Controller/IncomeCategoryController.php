<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Utils\CategoryHelper;
use App\Utils\MainHelper;
use App\Entity\IncomeCategory;
use Symfony\Component\HttpFoundation\Request;
use App\Forms\IncomeCategoryType;

/**
 * IncomeCategoryController
 *
 * @category Class
 * @package  Budget-tracker
 * @author   "Piotr Buczkowski <piotr.buczkowski@espeo.eu>"
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://github.com/bogorya/budget-tracker
 */
class IncomeCategoryController extends Controller
{
    /**
     * @Route("/income/category", name="income_category")
     * 
     * Adding new income category
     * 
     * @param CategoryHelper $helper instance of CategoryHelper class
     * @param Request $request request
     * 
     * @return Twig
     */
    public function index(CategoryHelper $helper, MainHelper $mainhelper, Request $request)
    {
        $icat = new IncomeCategory();
        $form = $this->createForm(IncomeCategoryType::class, $icat);
        $form->add('save', SubmitType::class);
        $form->handleRequest($request);
        $dates = $mainhelper->currentPeriod(1);
        if ($form->isSubmitted() && $form->isValid()) {
            $icat = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($icat);
            try {
                $entityManager->flush();
                return $this->redirectToRoute('income_category');
            } catch (\Exception $e) {
                return $this->render('errordb.html.twig', [
                    'current_period' => $dates,
                    'exception' => $e->getMessage()
                ]);
            }
        }
        try {
            $icats = $helper->getAllICategories();
            return $this->render('income_category/index.html.twig', [
                'current_period' => $dates,
                'form' => $form->createView(),
                'categories' => $icats
            ]);
        } catch (\Exception $e) {
            return $this->render('errordb2.html.twig', [
                'current_period' => $dates,
                'exception' => $e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/income/category/{id}", name="income_category_edit")
     * 
     * Updating income category
     * 
     * @param int $id ID of the category
     * @param CategoryHelper $helper instance of CategoryHelper class
     * @param Request $request request
     * 
     * @return Twig
     */
    public function edit(int $id, CategoryHelper $helper, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $icat = $entityManager->getRepository('App:IncomeCategory')->find($id);
        $form = $this->createForm(IncomeCategoryType::class, $icat);
        $form->add('update', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $entityManager->flush();
                return $this->redirectToRoute('income_category');
            } catch (\Exception $e) {
                return $this->render('errordb.html.twig', [
                    'current_period' => $helper->currentPeriod(),
                    'exception' => $e->getMessage()
                ]);
            }
        }
        return $this->render('income_category/edit.html.twig', [
            'current_period' => $helper->currentPeriod(),
            'form' => $form->createView()
        ]);
    }
}
