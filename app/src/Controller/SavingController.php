<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Utils\MainHelper;
use App\Utils\SavingHelper;
use App\Entity\Saving;
use Symfony\Component\HttpFoundation\Request;
use App\Forms\SavingType;

/**
 * SavingController
 *
 * @category Class
 * @package  Budget-tracker
 * @author   "Piotr Buczkowski <piotr.buczkowski@espeo.eu>"
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://github.com/bogorya/budget-tracker
 */
class SavingController extends Controller
{
    /**
     * @Route("/saving", name="saving")
     * 
     * Adding new saving
     * 
     * @param MainHelper $mainhelper instance of MainHelper class
     * @param Request $request request
     * 
     * @return Twig
     */
    public function index(SavingHelper $helper, MainHelper $mainhelper, Request $request)
    {
        $saving = new Saving();
        $form = $this->createForm(SavingType::class, $saving);
        $form->add('save', SubmitType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $saving = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($saving);
            try {
                $entityManager->flush();
                return $this->redirectToRoute('saving');
            } catch (\Exception $e) {
                return $this->render('errordb.html.twig', [
                    'current_period' => $mainhelper->currentPeriod(1),
                    'exception' => $e->getMessage()
                ]);
            }
        }
        try {
            $saves = $helper->getAllSavings();
            return $this->render('saving/index.html.twig', [
                'current_period' => $mainhelper->currentPeriod(1),
                'form' => $form->createView(),
                'savings' => $saves
            ]);
        } catch (\Exception $e) {
            return $this->render('errordb2.html.twig', [
                'current_period' => $mainhelper->currentPeriod(1),
                'exception' => $e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/saving/{id}", name="saving_edit")
     * 
     * Updating saving
     * 
     * @param int $id ID of the saving
     * @param MainHelper $mainhelper instance of MainHelper class
     * @param Request $request request
     * 
     * @return Twig
     */
    public function edit(int $id, MainHelper $mainhelper, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $saving = $entityManager->getRepository('App:Saving')->find($id);
        $form = $this->createForm(SavingType::class, $saving);
        $form->add('update', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $entityManager->flush();
                return $this->redirectToRoute('saving');
            } catch (\Exception $e) {
                return $this->render('errordb.html.twig', [
                    'current_period' => $mainhelper->currentPeriod(1),
                    'exception' => $e->getMessage()
                ]);
            }
        }
        return $this->render('saving/edit.html.twig', [
            'current_period' => $mainhelper->currentPeriod(1),
            'form' => $form->createView()
        ]);
    }
}
