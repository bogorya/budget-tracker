<?php
// src/Controller/IncomeCategoryApiController.php
namespace App\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use App\Entity\IncomeCategory;
use FOS\RestBundle\Controller\FOSRestController;
use App\Utils\ValidationHelper;
use App\Utils\ApiHelper;

/**
 * IncomeCategoryApiController
 *
 * @category Class
 * @package  Budget-tracker
 * @author   "Piotr Buczkowski <piotr.buczkowski@espeo.eu>"
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://github.com/bogorya/budget-tracker
 *
 * @Route("/api")
 */
class IncomeCategoryApiController extends FOSRestController
{
    /**
     * Create IncomeCategory.
     * @FOSRest\Post("/addincomecategory")
     *
     * @return JsonResponse
     */
    public function postIncomeCategoryAction(Request $request, ValidationHelper $valhelper): JsonResponse
    {
        $validate = json_decode($valhelper->validateNameInput($request)->getContent(), true);
        if ($validate['status'] != Response::HTTP_OK) {
            return new JsonResponse(array('status' => $validate['status'], 'data' => $validate['data']));
        } else {
            $incomecategory = new IncomeCategory();
            $incomecategory->setCategoryName($request->get('name'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($incomecategory);
            try {
                $em->flush();
            } catch (\Exception $e) {
                return new JsonResponse(
                    array(
                        'status' => Response::HTTP_BAD_REQUEST, 
                        'data' => [$e->getMessage()]
                    )
                );
            }
            return new JsonResponse(array('status' => Response::HTTP_OK, 'data' => ApiHelper::getContent($incomecategory)));
        }
    }

    /**
     * Remove IncomeCategory.
     * @FOSRest\Delete("/incomecategory/{incomecategoryId}")
     *
     * @return JsonResponse
     */
    public function deleteIncomeCategoryAction(Request $request, int $incomecategoryId): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $incomecategory = $em->getRepository('App:IncomeCategory')->find($incomecategoryId);
            $em->remove($incomecategory);
            $em->flush();
        } catch (\Exception $e) {
            return new JsonResponse(
                array(
                    'status' => Response::HTTP_BAD_REQUEST, 
                    'data' => [$e->getMessage()]
                )
            );
        }
        return new JsonResponse(array('status' => Response::HTTP_OK, 'data' => []));
    }

    /**
     * Get IncomeCategory.
     * @FOSRest\Get("/incomecategory/{incomecategoryId}")
     *
     * @return JsonResponse
     */
    public function getIncomeCategoryAction(int $incomecategoryId): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $incomecategory = $em->getRepository('App:IncomeCategory')->find($incomecategoryId);
        } catch (\Exception $e) {
            return new JsonResponse(
                array(
                    'status' => Response::HTTP_BAD_REQUEST, 
                    'data' => [$e->getMessage()]
                )
            );
        }
        return new JsonResponse(array('status' => Response::HTTP_OK, 'data' => ApiHelper::getContent($incomecategory)));
    }

    /**
     * Update IncomeCategory.
     * @FOSRest\Put("/incomecategory/{incomecategoryId}")
     *
     * @return JsonResponse
     */
    public function putIncomeCategoryAction(int $incomecategoryId, Request $request, ValidationHelper $valhelper): JsonResponse
    {
        $validate = json_decode($valhelper->validateNameInput($request)->getContent(), true);
        if ($validate['status'] != Response::HTTP_OK) {
            return new JsonResponse(array('status' => $validate['status'], 'data' => $validate['data']));
        } else {
            $em = $this->getDoctrine()->getManager();
            try {
                $incomecategory = $em->getRepository('App:IncomeCategory')->find($incomecategoryId);
            } catch (\Exception $e) {
                return new JsonResponse(
                    array(
                        'status' => Response::HTTP_BAD_REQUEST, 
                        'data' => [$e->getMessage()]
                    )
                );
            }
            $incomecategory->setCategoryName($request->get('name'));
            try {
                $em->persist($incomecategory);
                $em->flush();
            } catch (\Exception $e) {
                return new JsonResponse(
                    array(
                        'status' => Response::HTTP_BAD_REQUEST, 
                        'data' => [$e->getMessage()]
                    )
                );
            }   
            return new JsonResponse(array('status' => Response::HTTP_OK, 'data' => ApiHelper::getContent($incomecategory)));
        }
    }

}