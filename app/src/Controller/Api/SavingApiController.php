<?php
// src/Controller/SavingApiController.php
namespace App\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use App\Entity\Saving;
use FOS\RestBundle\Controller\FOSRestController;
use App\Utils\ApiHelper;
use App\Utils\ValidationHelper;

/**
 * SavingApiController
 *
 * @category Class
 * @package  Budget-tracker
 * @author   "Piotr Buczkowski <piotr.buczkowski@espeo.eu>"
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://github.com/bogorya/budget-tracker
 *
 * @Route("/api")
 */
class SavingApiController extends FOSRestController
{
    /**
     * Create Saving.
     * @FOSRest\Post("/addsaving")
     *
     * @return JsonResponse
     */
    public function postSavingAction(Request $request, ValidationHelper $valhelper): JsonResponse
    {
        $validate = json_decode($valhelper->validateAmountInput($request)->getContent(), true);
        if ($validate['status'] != Response::HTTP_OK) {
            return new JsonResponse(array('status' => $validate['status'], 'data' => $validate['data']));
        } else {
            $saving = new Saving();
            $saving->setSavingAmount($request->get('amount'));
            $saving->setSavingStamp(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $em->persist($saving);
            try {
                $em->flush();
            } catch (\Exception $e) {
                return new JsonResponse(
                    array(
                        'status' => Response::HTTP_BAD_REQUEST, 
                        'data' => [$e->getMessage()]
                    )
                );
            }
            return new JsonResponse(array('status' => Response::HTTP_OK, 'data' => ApiHelper::getContent($saving)));
        }
    }

    /**
     * Remove Saving.
     * @FOSRest\Delete("/saving/{savingId}")
     *
     * @return JsonResponse
     */
    public function deleteSavingAction(Request $request, int $savingId): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $saving = $em->getRepository('App:Saving')->find($savingId);
            $em->remove($saving);
            $em->flush();
        } catch (\Exception $e) {
            return new JsonResponse(
                array(
                    'status' => Response::HTTP_BAD_REQUEST, 
                    'data' => [$e->getMessage()]
                )
            );
        }
        return new JsonResponse(array('status' => Response::HTTP_OK, 'data' => []));
    }

    /**
     * Get Saving.
     * @FOSRest\Get("/saving/{savingId}")
     *
     * @return JsonResponse
     */
    public function getSavingAction(int $savingId): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $saving = $em->getRepository('App:Saving')->find($savingId);
        } catch (\Exception $e) {
            return new JsonResponse(
                array(
                    'status' => Response::HTTP_BAD_REQUEST, 
                    'data' => [$e->getMessage()]
                )
            );
        }
        return new JsonResponse(array('status' => Response::HTTP_OK, 'data' => ApiHelper::getContent($saving)));
    }

   /**
     * Update Saving.
     * @FOSRest\Put("/saving/{savingId}")
     *
     * @return JsonResponse
     */
    public function putSavingAction(int $savingId, Request $request, ValidationHelper $valhelper): JsonResponse
    {
        $validate = json_decode($valhelper->validateAmountInput($request)->getContent(), true);
        if ($validate['status'] != Response::HTTP_OK) {
            return new JsonResponse(array('status' => $validate['status'], 'data' => $validate['data']));
        } else {
            $em = $this->getDoctrine()->getManager();
            try {
                $saving = $em->getRepository('App:Saving')->find($savingId);
            } catch (\Exception $e) {
                return new JsonResponse(
                    array(
                        'status' => Response::HTTP_BAD_REQUEST, 
                        'data' => [$e->getMessage()]
                    )
                );
            }
            $saving->setSavingAmount($request->get('amount'));
            $saving->setSavingStamp(new \DateTime());
            try {
                $em->persist($saving);
                $em->flush();
            } catch (\Exception $e) {
                return new JsonResponse(
                    array(
                        'status' => Response::HTTP_BAD_REQUEST, 
                        'data' => [$e->getMessage()]
                    )
                );
            }   
            return new JsonResponse(array('status' => Response::HTTP_OK, 'data' => ApiHelper::getContent($saving)));
        }
    }

}