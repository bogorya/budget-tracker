<?php
// src/Controller/ExpenseCategoryApiController.php
namespace App\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use App\Entity\ExpenseCategory;
use FOS\RestBundle\Controller\FOSRestController;
use App\Utils\ValidationHelper;
use App\Utils\ApiHelper;


/**
 * ExpenseCategoryApiController
 *
 * @category Class
 * @package  Budget-tracker
 * @author   "Piotr Buczkowski <piotr.buczkowski@espeo.eu>"
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://github.com/bogorya/budget-tracker
 *
 * @Route("/api")
 */
class ExpenseCategoryApiController extends FOSRestController
{
    /**
     * Create ExpenseCategory.
     * @FOSRest\Post("/addexpensecategory")
     *
     * @return JsonResponse
     */
    public function postExpenseCategoryAction(Request $request, ValidationHelper $valhelper): JsonResponse
    {
        $validate = json_decode($valhelper->validateNameInput($request)->getContent(), true);
        if ($validate['status'] != Response::HTTP_OK) {
            return new JsonResponse(array('status' => $validate['status'], 'data' => $validate['data']));
        } else {
            $expensecategory = new ExpenseCategory();
            $expensecategory->setCategoryName($request->get('name'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($expensecategory);
            try {
                $em->flush();
            } catch (\Exception $e) {
                return new JsonResponse(
                    array(
                        'status' => Response::HTTP_BAD_REQUEST, 
                        'data' => [$e->getMessage()]
                    )
                );
            }
            return new JsonResponse(array('status' => Response::HTTP_OK, 'data' => ApiHelper::getContent($expensecategory)));
        }
    }

    /**
     * Remove ExpenseCategory.
     * @FOSRest\Delete("/expensecategory/{expensecategoryId}")
     *
     * @return JsonResponse
     */
    public function deleteExpenseCategoryAction(Request $request, int $expensecategoryId): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $expensecategory = $em->getRepository('App:ExpenseCategory')->find($expensecategoryId);
            $em->remove($expensecategory);
            $em->flush();
        } catch (\Exception $e) {
            return new JsonResponse(
                array(
                    'status' => Response::HTTP_BAD_REQUEST, 
                    'data' => [$e->getMessage()]
                )
            );
        }
        return new JsonResponse(array('status' => Response::HTTP_OK, 'data' => []));
    }

    /**
     * Get ExpenseCategory.
     * @FOSRest\Get("/expensecategory/{expensecategoryId}")
     *
     * @return JsonResponse
     */
    public function getExpenseCategoryAction(int $expensecategoryId): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $expensecategory = $em->getRepository('App:ExpenseCategory')->find($expensecategoryId);
        } catch (\Exception $e) {
            return new JsonResponse(
                array(
                    'status' => Response::HTTP_BAD_REQUEST, 
                    'data' => [$e->getMessage()]
                )
            );
        }
        return new JsonResponse(array('status' => Response::HTTP_OK, 'data' => ApiHelper::getContent($expensecategory)));
    }

   /**
     * Update ExpenseCategory.
     * @FOSRest\Put("/expensecategory/{expensecategoryId}")
     *
     * @return JsonResponse
     */
    public function putExpenseCategoryAction(int $expensecategoryId, Request $request, ValidationHelper $valhelper): JsonResponse
    {
        $validate = json_decode($valhelper->validateNameInput($request)->getContent(), true);
        if ($validate['status'] != Response::HTTP_OK) {
            return new JsonResponse(array('status' => $validate['status'], 'data' => $validate['data']));
        } else {
            $em = $this->getDoctrine()->getManager();
            try {
                $expensecategory = $em->getRepository('App:ExpenseCategory')->find($expensecategoryId);
            } catch (\Exception $e) {
                return new JsonResponse(
                    array(
                        'status' => Response::HTTP_BAD_REQUEST, 
                        'data' => [$e->getMessage()]
                    )
                );
            }
            $expensecategory->setCategoryName($request->get('name'));
            try {
                $em->persist($expensecategory);
                $em->flush();
            } catch (\Exception $e) {
                return new JsonResponse(
                    array(
                        'status' => Response::HTTP_BAD_REQUEST, 
                        'data' => [$e->getMessage()]
                    )
                );
            }   
            return new JsonResponse(array('status' => Response::HTTP_OK, 'data' => ApiHelper::getContent($expensecategory)));
        }
    }

}