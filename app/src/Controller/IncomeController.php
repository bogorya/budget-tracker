<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Utils\IncomeHelper;
use App\Utils\MainHelper;
use App\Entity\Income;
//use App\Repository\IncomeRepo;
use Symfony\Component\HttpFoundation\Request;
use App\Forms\IncomeType;
use Doctrine\ORM\EntityManager;

/**
 * IncomeController
 *
 * @category Class
 * @package  Budget-tracker
 * @author   "Piotr Buczkowski <piotr.buczkowski@espeo.eu>"
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://github.com/bogorya/budget-tracker
 */
class IncomeController extends Controller
{
    /**
     * @Route("/income", name="income")
     * 
     * Adding new income
     * 
     * @param IncomeHelper $helper instance of IncomeHelper class
     * @param Request $request request
     * 
     * @return Twig
     */
    public function index(IncomeHelper $helper, MainHelper $mainhelper, Request $request)
    {
        $income = new Income();
        $form = $this->createForm(IncomeType::class, $income);
        $form->get('incomeStamp')->setData(new \DateTime);
        $form->add('save', SubmitType::class);
        $form->handleRequest($request);
        //$mainhelper = new MainHelper();
        //$helper = new IncomeHelper();
        if ($form->isSubmitted() && $form->isValid()) {
            $income = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($income);
            try {
                $entityManager->flush();
                return $this->redirectToRoute('income');
            } catch (\Exception $e) {
                return $this->render('errordb.html.twig', [
                    'current_period' => $mainhelper->currentPeriod(1),
                    'exception' => $e->getMessage()
                ]);
            }
        }
        try {
            $incomes = $helper->getAllIncomes();
            return $this->render('income/index.html.twig', [
                'current_period' => $mainhelper->currentPeriod(1),
                'form' => $form->createView(),
                'incomes' => $incomes
            ]);
        } catch (\Exception $e) {
            return $this->render('errordb2.html.twig', [
                'current_period' => $mainhelper->currentPeriod(1),
                'exception' => $e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/income/{id}", name="income_edit")
     * 
     * Updating income
     * 
     * @param int $id ID of the income
     * @param IncomeHelper $helper instance of IncomeHelper class
     * @param Request $request request
     * 
     * @return Twig
     */
    public function edit(int $id, IncomeHelper $helper, MainHelper $mainhelper, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $income = $entityManager->getRepository('App:Income')->find($id);
        $form = $this->createForm(IncomeType::class, $income);
        $form->add('update', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $entityManager->flush();
                return $this->redirectToRoute('income');
            } catch (\Exception $e) {
                return $this->render('errordb.html.twig', [
                    'current_period' => $mainhelper->currentPeriod(1),
                    'exception' => $e->getMessage()
                ]);
            }
        }
        return $this->render('income/edit.html.twig', [
            'current_period' => $mainhelper->currentPeriod(1),
            'form' => $form->createView()
        ]);
    }
}
