<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Utils\CategoryHelper;
use App\Utils\MainHelper;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\ExpenseCategory;
use App\Entity\Limits;
use Symfony\Component\HttpFoundation\Request;
use App\Forms\ExpenseCategoryType;

/**
 * ExpenseCategoryController
 *
 * @category Class
 * @package  Budget-tracker
 * @author   "Piotr Buczkowski <piotr.buczkowski@espeo.eu>"
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://github.com/bogorya/budget-tracker
 */
class ExpenseCategoryController extends Controller
{
    /**
     * @Route("/expense/category", name="expense_category")
     * 
     * Adding new expense category
     * 
     * @param CategoryHelper $helper instance of CategoryHelper class
     * @param Request $request request
     * 
     * @return Twig
     */
    public function index(CategoryHelper $helper, MainHelper $mainhelper, Request $request)
    {
        $ecat = new ExpenseCategory();
        $dates = $mainhelper->currentPeriod(1);
        $form = $this->createForm(ExpenseCategoryType::class, $ecat);
        $form->add('save', SubmitType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $ecat = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ecat);
            //limits
            $sentData = $request->get('expense_category');
            $newLimits = new Limits();
            $newLimits->setLimitsAmount($sentData['limitsAmount']);
            $newLimits->setLimitsActive(
                !isset($sentData['limitsActive'])?false:$sentData['limitsActive']
            );
            $newLimits->setLimitsCategory($ecat->getId());
            $newLimits->setLimitsDate(new \DateTime($dates[0]));
            $entityManager->persist($newLimits);
            try {
                $entityManager->flush();
                return $this->redirectToRoute('expense_category');
            } catch (\Exception $e) {
                return $this->render('errordb.html.twig', [
                    'current_period' => $dates,
                    'exception' => $e->getMessage()
                ]);
            }
        }

        try {
            $ecats = $helper->getAllECategoriesWithLimits(new \DateTime($dates[0]));
            return $this->render('expense_category/index.html.twig', [
                'current_period' => $dates,
                'form' => $form->createView(),
                'categories' => $ecats
            ]);
        } catch (\Exception $e) {
            return $this->render('errordb2.html.twig', [
                'current_period' => $dates,
                'exception' => $e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/expense/category/{id}", name="expense_category_edit")
     * 
     * Updating expense category
     * 
     * @param int $id ID of the category
     * @param CategoryHelper $helper instance of CategoryHelper class
     * @param Request $request request
     * 
     * @return Twig
     */
    public function edit(int $id, CategoryHelper $helper, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $dates = $helper->currentPeriod();
        $ecat = $entityManager->getRepository('App:ExpenseCategory')->findOneWithLimits($id, new \DateTime($dates[0]));
        $ecobject = new ExpenseCategory();
        $ecobject->setCategoryName($ecat['categoryName']);
        $ecobject->setLimitsAmount($ecat['limitsAmount']);
        $ecobject->setLimitsActive(
            !isset($ecat['limitsActive'])?false:$ecat['limitsActive']
        );
        $form = $this->createForm(ExpenseCategoryType::class, $ecobject);
        $form->add('update', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $entityManager->flush();
                return $this->redirectToRoute('expense_category');
            } catch (\Exception $e) {
                return $this->render('errordb.html.twig', [
                    'current_period' => $dates,
                    'exception' => $e->getMessage()
                ]);
            }
        }
        return $this->render('expense_category/edit.html.twig', [
            'current_period' => $dates,
            'form' => $form->createView()
        ]);
    }
}
