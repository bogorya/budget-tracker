<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180702123454 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER SEQUENCE expense_category_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE expense_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE income_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE income_category_id_seq INCREMENT BY 1');
        $this->addSql('CREATE SEQUENCE saving_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE saving (id INT NOT NULL, saving_amount DOUBLE PRECISION NOT NULL, saving_stamp DATE NOT NULL, saving_note VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER SEQUENCE income_category_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE expense_category_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE expense_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE income_id_seq INCREMENT BY 1');
        $this->addSql('DROP SEQUENCE saving_id_seq CASCADE');
        $this->addSql('DROP TABLE saving');
    }
}
