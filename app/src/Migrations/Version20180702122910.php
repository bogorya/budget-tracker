<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180702122910 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER SEQUENCE expense_category_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE income_category_id_seq INCREMENT BY 1');
        $this->addSql('CREATE SEQUENCE expense_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE expense (id INT NOT NULL, expense_category_id_id INT NOT NULL, expense_amount DOUBLE PRECISION NOT NULL, expense_stamp DATE NOT NULL, expense_notes VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2D3A8DA6208758E0 ON expense (expense_category_id_id)');
        $this->addSql('ALTER TABLE expense ADD CONSTRAINT FK_2D3A8DA6208758E0 FOREIGN KEY (expense_category_id_id) REFERENCES expense_category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER SEQUENCE income_category_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE expense_category_id_seq INCREMENT BY 1');
        $this->addSql('DROP SEQUENCE expense_id_seq CASCADE');
        $this->addSql('DROP TABLE expense');
    }
}
