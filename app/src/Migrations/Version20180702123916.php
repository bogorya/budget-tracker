<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180702123916 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER SEQUENCE expense_category_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE saving_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE expense_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE income_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE income_category_id_seq INCREMENT BY 1');
        $this->addSql('CREATE SEQUENCE limits_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE "limits" (id INT NOT NULL, limits_category_id INT NOT NULL, limits_date DATE NOT NULL, limits_amount DOUBLE PRECISION NOT NULL, limits_active BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7F96E8601AF0F55C ON "limits" (limits_category_id)');
        $this->addSql('ALTER TABLE "limits" ADD CONSTRAINT FK_7F96E8601AF0F55C FOREIGN KEY (limits_category_id) REFERENCES expense_category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER SEQUENCE income_category_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE expense_category_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE expense_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE income_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE saving_id_seq INCREMENT BY 1');
        $this->addSql('DROP SEQUENCE limits_id_seq CASCADE');
        $this->addSql('DROP TABLE "limits"');
    }
}
