<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180706112347 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('INSERT INTO expense_category (id, category_name) VALUES (99, \'test category\')');
        $this->addSql('INSERT INTO limits (id, limits_category_id, limits_date, limits_amount, limits_active) VALUES (1, 99, \'2018-07-05\', 500.00, false)');
        $this->addSql('INSERT INTO expense (id, expense_category_id_id, expense_amount, expense_stamp, expense_notes) VALUES (1, 99, 200.00, \'2018-07-05\', \'Test note\')');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('TRUNCATE TABLE expense_category CASCADE');
        $this->addSql('TRUNCATE TABLE expense CASCADE');
        $this->addSql('TRUNCATE TABLE limits CASCADE');
    }
}
