<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180702123309 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER SEQUENCE expense_category_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE expense_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE income_category_id_seq INCREMENT BY 1');
        $this->addSql('CREATE SEQUENCE income_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE income (id INT NOT NULL, income_category_id INT NOT NULL, income_amount DOUBLE PRECISION NOT NULL, income_stamp DATE NOT NULL, income_notes VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3FA862D053F8702F ON income (income_category_id)');
        $this->addSql('ALTER TABLE income ADD CONSTRAINT FK_3FA862D053F8702F FOREIGN KEY (income_category_id) REFERENCES income_category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER SEQUENCE income_category_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE expense_category_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE expense_id_seq INCREMENT BY 1');
        $this->addSql('DROP SEQUENCE income_id_seq CASCADE');
        $this->addSql('DROP TABLE income');
    }
}
