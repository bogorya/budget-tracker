<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180702125205 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER SEQUENCE expense_category_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE saving_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE expense_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE income_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE limits_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE income_category_id_seq INCREMENT BY 1');
        $this->addSql('CREATE SEQUENCE options_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE "options" (id INT NOT NULL, options_name VARCHAR(255) NOT NULL, options_value INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO "options" (id, options_name, options_value) VALUES (1, \'day_first\', 1)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER SEQUENCE income_category_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE expense_category_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE expense_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE income_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE saving_id_seq INCREMENT BY 1');
        $this->addSql('ALTER SEQUENCE limits_id_seq INCREMENT BY 1');
        $this->addSql('DROP SEQUENCE options_id_seq CASCADE');
        $this->addSql('DROP TABLE "options"');
    }
}
