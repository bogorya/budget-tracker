<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SavingRepository")
 */
class Saving
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     * @Assert\Type("float")
     */
    private $savingAmount;

    /**
     * @ORM\Column(type="date")
     */
    private $savingStamp;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $savingNote;

    public function getId()
    {
        return $this->id;
    }

    public function getSavingAmount(): ?float
    {
        return $this->savingAmount;
    }

    public function setSavingAmount(float $savingAmount): self
    {
        $this->savingAmount = $savingAmount;

        return $this;
    }

    public function getSavingStamp(): ?\DateTimeInterface
    {
        return $this->savingStamp;
    }

    public function setSavingStamp(\DateTimeInterface $savingStamp): self
    {
        $this->savingStamp = $savingStamp;

        return $this;
    }

    public function getSavingNote(): ?string
    {
        return $this->savingNote;
    }

    public function setSavingNote(?string $savingNote): self
    {
        $this->savingNote = $savingNote;

        return $this;
    }
}
