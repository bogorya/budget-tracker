<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
//use Doctrine\ORM\EntityRepository;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IncomeRepository")
 */
class Income
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $incomeAmount;

    /**
     * @ORM\Column(type="date")
     */
    private $incomeStamp;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\IncomeCategory", inversedBy="incomes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $incomeCategory;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $incomeNotes;

    public function getId()
    {
        return $this->id;
    }

    public function getIncomeAmount(): ?float
    {
        return $this->incomeAmount;
    }

    public function setIncomeAmount(float $incomeAmount): self
    {
        $this->incomeAmount = $incomeAmount;

        return $this;
    }

    public function getIncomeStamp(): ?\DateTimeInterface
    {
        return $this->incomeStamp;
    }

    public function setIncomeStamp(\DateTimeInterface $incomeStamp): self
    {
        $this->incomeStamp = $incomeStamp;

        return $this;
    }

    public function getIncomeCategory(): ?incomeCategory
    {
        return $this->incomeCategory;
    }

    public function setIncomeCategory(?incomeCategory $incomeCategory): self
    {
        $this->incomeCategory = $incomeCategory;

        return $this;
    }

    public function getIncomeNotes(): ?string
    {
        return $this->incomeNotes;
    }

    public function setIncomeNotes(?string $incomeNotes): self
    {
        $this->incomeNotes = $incomeNotes;

        return $this;
    }
}