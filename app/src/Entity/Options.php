<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OptionsRepository")
 */
class Options
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $optionsName;

    /**
     * @ORM\Column(type="integer")
     */
    private $optionsValue;

    public function getId()
    {
        return $this->id;
    }

    public function getOptionName(): ?string
    {
        return $this->optionsName;
    }

    public function setOptionName(string $optionsName): self
    {
        $this->optionsName = $optionsName;

        return $this;
    }

    public function getOptionValue(): ?int
    {
        return $this->optionsValue;
    }

    public function setOptionValue(int $optionsValue): self
    {
        $this->optionsValue = $optionsValue;

        return $this;
    }
}
