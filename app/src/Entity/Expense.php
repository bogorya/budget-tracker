<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExpenseRepository")
 */
class Expense
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $expenseAmount;

    /**
     * @ORM\Column(type="date")
     */
    private $expenseStamp;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ExpenseCategory", inversedBy="expenses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $expenseCategoryId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $expenseNotes;

    public function getId()
    {
        return $this->id;
    }

    public function getExpenseAmount(): ?float
    {
        return $this->expenseAmount;
    }

    public function setExpenseAmount(float $expenseAmount): self
    {
        $this->expenseAmount = $expenseAmount;

        return $this;
    }

    public function getExpenseStamp(): ?\DateTimeInterface
    {
        return $this->expenseStamp;
    }

    public function setExpenseStamp(\DateTimeInterface $expenseStamp): self
    {
        $this->expenseStamp = $expenseStamp;

        return $this;
    }

    public function getExpenseCategoryId(): ?expenseCategory
    {
        return $this->expenseCategoryId;
    }

    public function setExpenseCategoryId(?expenseCategory $expenseCategoryId): self
    {
        $this->expenseCategoryId = $expenseCategoryId;

        return $this;
    }

    public function getExpenseNotes(): ?string
    {
        return $this->expenseNotes;
    }

    public function setExpenseNotes(?string $expenseNotes): self
    {
        $this->expenseNotes = $expenseNotes;

        return $this;
    }
}
