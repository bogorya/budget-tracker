<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LimitsRepository")
 */
class Limits
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $limitsDate;

    /**
     * @ORM\Column(type="float")
     */
    private $limitsAmount;

    /**
     * @ORM\Column(type="integer")
     */
    private $limitsCategory;

    /**
     * @ORM\Column(type="boolean")
     */
    private $limitsActive;

    public function getId()
    {
        return $this->id;
    }

    public function getLimitsDate(): ?\DateTimeInterface
    {
        return $this->limitsDate;
    }

    public function setLimitsDate(\DateTimeInterface $limitsDate): self
    {
        $this->limitsDate = $limitsDate;

        return $this;
    }

    public function getLimitsAmount(): ?float
    {
        return $this->limitsAmount;
    }

    public function setLimitsAmount(float $limitsAmount): self
    {
        $this->limitsAmount = $limitsAmount;

        return $this;
    }
    
    public function getLimitsCategory(): int
    {
        return $this->limitsCategory;
    }

    public function setLimitsCategory(int $limitsCategory): self
    {
        $this->limitsCategory = $limitsCategory;

        return $this;
    }

    public function getLimitsActive(): ?bool
    {
        return $this->limitsActive;
    }

    public function setLimitsActive(bool $limitsActive): self
    {
        $this->limitsActive = $limitsActive;

        return $this;
    }
}
