<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExpenseCategoryRepository")
 */
class ExpenseCategory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Type("String")
     */
    private $categoryName;

    /** 
    * @ORM\OneToMany(targetEntity="App\Entity\Expense", mappedBy="expenseCategoryId")
    */
    private $expenses;

    ///**
    // * @ORM\OneToMany(targetEntity="App\Entity\Limit", mappedBy="limitCategory")
    // */
    private $limitsAmount;

    ///**
    // * @ORM\OneToMany(targetEntity="App\Entity\Limit", mappedBy="limitActive")
    // */
    private $limitsActive;

    public function __construct()
    {
        //$this->expenses = new ArrayCollection();
        //$this->limit = 0;
        //$this->limit_active = false;
        //$this->limitNA = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCategoryName(): ?string
    {
        return $this->categoryName;
    }

    public function setCategoryName(string $categoryName): self
    {
        $this->categoryName = $categoryName;

        return $this;
    }

    public function getLimitsAmount(): ?string
    {
        return $this->limitsAmount;
    }

    public function setLimitsAmount(float $amount): self
    {
        $this->limitsAmount = $amount;

        return $this;
    }

    public function getLimitsActive(): ?string
    {
        return $this->limitsActive;
    }

    public function setLimitsActive(bool $active): self
    {
        $this->limitsActive = $active;

        return $this;
    }

    ///**
    /// * @return Collection|Expense[]
     //*/
    /*public function getExpenses(): Collection
    {
        return $this->expenses;
    }
    
    public function addExpense(Expense $expense): self
    {
        if (!$this->expenses->contains($expense)) {
            $this->expenses[] = $expense;
            $expense->setExpenseCategoryId($this);
        }

        return $this;
    }

    public function removeExpense(Expense $expense): self
    {
        if ($this->expenses->contains($expense)) {
            $this->expenses->removeElement($expense);
            // set the owning side to null (unless already changed)
            if ($expense->getExpenseCategoryId() === $this) {
                $expense->setExpenseCategoryId(null);
            }
        }

        return $this;
    }
    */
    ///**
    // * @return float
    // */
    /*public function getLimit(): float
    {
        return $this->limit;
    }

    public function setLimit(float $limit): self
    {
        $this->limit = $limit;

        return $this;
    }
*/
    ///**
    // * @return Boolean
    // */
    /*public function getLimitActive(): bool
    {
        return $this->limit_active;
    }

    public function setLimitActive(bool $active): self
    {
        $this->limit_active = $active;
        return $this;
    }
    */
    /*public function addLimitNA(Limit $limitNA): self
    {
        if (!$this->limitNA->contains($limitNA)) {
            $this->limitNA[] = $limitNA;
            $limitNA->setLimitCategory($this);
        }

        return $this;
    }

    public function removeLimitNA(Limit $limitNA): self
    {
        if ($this->limitNA->contains($limitNA)) {
            $this->limitNA->removeElement($limitNA);
            // set the owning side to null (unless already changed)
            if ($limitNA->getLimitCategory() === $this) {
                $limitNA->setLimitCategory(null);
            }
        }

        return $this;
    }
    */
}
