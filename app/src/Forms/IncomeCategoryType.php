<?php

namespace App\Forms;

use Symfony\Component\Form\AbstractType;
use App\Entity\IncomeCategory;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * IncomeCategoryType
 *
 * @category Class
 * @package  Budget-tracker
 * @author   "Piotr Buczkowski <piotr.buczkowski@espeo.eu>"
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://github.com/bogorya/budget-tracker
 */
class IncomeCategoryType extends AbstractType
{
    /**
     * Form builder for saving
     * 
     * @param FormBuilderInterface $builder instance of FormBuilderInterface
     * @param array $options options
     * 
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('categoryName', TextType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => IncomeCategory::class
        ]);
    }
}
