<?php

namespace App\Forms;

use Symfony\Component\Form\AbstractType;
use App\Entity\Saving;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * SavingType
 *
 * @category Class
 * @package  Budget-tracker
 * @author   "Piotr Buczkowski <piotr.buczkowski@espeo.eu>"
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://github.com/bogorya/budget-tracker
 */
class SavingType extends AbstractType
{
    /**
     * Form builder for income category
     * 
     * @param FormBuilderInterface $builder instance of FormBuilderInterface
     * @param array $options options
     * 
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('savingAmount', MoneyType::class, ['currency' => 'PLN'])
            ->add('savingStamp', DateType::class, [ 'data' => new \DateTime()])
            ->add('savingNote', TextType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Saving::class
        ]);
    }
}
