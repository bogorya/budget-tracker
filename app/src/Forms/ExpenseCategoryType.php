<?php

namespace App\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Validator\Constraints\NotBlank;
use App\Entity\ExpenseCategory;

/**
 * ExpenseCategoryType
 *
 * @category Class
 * @package  Budget-tracker
 * @author   "Piotr Buczkowski <piotr.buczkowski@espeo.eu>"
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://github.com/bogorya/budget-tracker
 */
class ExpenseCategoryType extends AbstractType
{
    /**
     * Form builder for expense category
     * 
     * @param FormBuilderInterface $builder instance of FormBuilderInterface
     * @param array $options options
     * 
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('categoryName', TextType::class)
            ->add('limitsAmount', MoneyType::class, ['mapped' => false, 'constraints' => new NotBlank(), 'currency' => 'PLN'])
            ->add('limitsActive', CheckboxType::class, ['mapped' => false, 'required' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ExpenseCategory::class
        ]);
    }
}
