<?php

namespace App\Forms;

use Symfony\Component\Form\AbstractType;
use App\Entity\Expense;
use App\Entity\ExpenseCategory;
use App\Utils\BTHelper;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
/**
 * ExpenseType
 *
 * @category Class
 * @package  Budget-tracker
 * @author   "Piotr Buczkowski <piotr.buczkowski@espeo.eu>"
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://github.com/bogorya/budget-tracker
 */

class ExpenseType extends AbstractType
{
    /**
     * Form builder for expense
     * 
     * @param FormBuilderInterface $builder instance of FormBuilderInterface
     * @param array $options options
     * 
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('expenseCategoryId', EntityType::class, [
                'class' => ExpenseCategory::class, 
                'choice_label' => 'categoryName'
            ])
            ->add('expenseAmount', MoneyType::class, ['currency' => 'PLN'])
            ->add('expenseStamp', DateType::class)
            ->add('expenseNotes', TextType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Expense::class
        ]);
    }
}
