<?php

namespace App\Forms;

use Symfony\Component\Form\AbstractType;
use App\Entity\Income;
use App\Entity\IncomeCategory;
use App\Utils\BTHelper;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * IncomeType
 *
 * @category Class
 * @package  Budget-tracker
 * @author   "Piotr Buczkowski <piotr.buczkowski@espeo.eu>"
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://github.com/bogorya/budget-tracker
 */
class IncomeType extends AbstractType
{
    /**
     * Form builder for income
     * 
     * @param FormBuilderInterface $builder instance of FormBuilderInterface
     * @param array $options options
     * 
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('incomeCategory', EntityType::class, [
                'class' => IncomeCategory::class, 
                'choice_label' => 'categoryName'
            ])
            ->add('incomeAmount', MoneyType::class, ['currency' => 'PLN'])
            ->add('incomeStamp', DateType::class)
            ->add('incomeNotes', TextType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Income::class
        ]);
    }
}
