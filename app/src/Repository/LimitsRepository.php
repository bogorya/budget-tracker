<?php

namespace App\Repository;

use App\Entity\Limits;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Limits|null find($id, $lockMode = null, $lockVersion = null)
 * @method Limits|null findOneBy(array $criteria, array $orderBy = null)
 * @method Limits[]    findAll()
 * @method Limits[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LimitsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Limits::class);
    }

    public function findSum(array $dates): ?float
    {
        $lims = $this->createQueryBuilder('i')
            ->select('SUM(i.limitsAmount) AS limsum')
            ->andwhere('i.limitsDate BETWEEN :date1 AND :date2')
            ->andwhere('i.limitsActive = false')
            ->setParameter('date1', $dates[0])
            ->setParameter('date2', $dates[1])
            ->setMaxResults(1)
            ->getQuery();

        $res = $lims->execute();
        return $res[0]['limsum'];
    }

    public function getLimitValue(int $catId, array $dates): ?float
    {
        try {

            $lims = $this->createQueryBuilder('i')
                ->select('limitsAmount as limvalue')
                ->andwhere('i.limitsDate BETWEEN :date1 AND :date2')
                ->andwhere('i.limitsCategory = :catid')
                ->andwhere('i.limitsActive = false')
                ->setParameter('catid', $catId)
                ->setParameter('date1', $dates[0])
                ->setParameter('date2', $dates[1])
                ->setMaxResults(1)
                ->getQuery();

            $res = $lims->execute();
            return $res[0]['limvalue'];
        } catch(\Exception $e) {
            return 0;
        }
    }
}
