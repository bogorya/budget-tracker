<?php

namespace App\Repository;

use App\Entity\Saving;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Saving|null find($id, $lockMode = null, $lockVersion = null)
 * @method Saving|null findOneBy(array $criteria, array $orderBy = null)
 * @method Saving[]    findAll()
 * @method Saving[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SavingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Saving::class);
    }

    public function findSum(array $dates): ?float
    {
        $inc = $this->createQueryBuilder('i')
            ->select('SUM(i.savingAmount) AS savsum')
            ->where('i.savingStamp BETWEEN :date1 AND :date2')
            ->setParameter('date1', $dates[0])
            ->setParameter('date2', $dates[1])
            ->setMaxResults(1)
            ->getQuery();

        $res = $inc->execute();
        return $res[0]['savsum'];
    }
}
