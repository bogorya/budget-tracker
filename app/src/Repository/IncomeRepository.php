<?php

namespace App\Repository;

use App\Entity\Income;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Income|null find($id, $lockMode = null, $lockVersion = null)
 * @method Income|null findOneBy(array $criteria, array $orderBy = null)
 * @method Income[]    findAll()
 * @method Income[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IncomeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Income::class);
    }
    
    public function findSum(array $dates): ?float
    {
        $inc = $this->createQueryBuilder('i')
            ->select('SUM(i.incomeAmount) AS incsum')
            ->where('i.incomeStamp BETWEEN :date1 AND :date2')
            ->setParameter('date1', $dates[0])
            ->setParameter('date2', $dates[1])
            ->setMaxResults(1)
            ->getQuery();

        $res = $inc->execute();
        return $res[0]['incsum'];
    }
    
}
