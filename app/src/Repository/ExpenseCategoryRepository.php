<?php

namespace App\Repository;

use App\Entity\ExpenseCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ExpenseCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExpenseCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExpenseCategory[]    findAll()
 * @method ExpenseCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExpenseCategoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ExpenseCategory::class);
    }

    public function findAllWithLimits(\DateTime $date): array
    {
        return $this->createQueryBuilder('e')
            ->select('e.id, e.categoryName', 'lim.limitsAmount', 'lim.limitsActive')
            ->leftJoin('App\Entity\Limits', 'lim', 'WITH', 'e.id = lim.limitsCategory')
            ->andWhere('lim.limitsDate = :date')
            ->setParameter('date', $date)
            ->getQuery()
            ->getResult()
        ;
    }

    
    public function findOneWithLimits(int $id, \DateTime $date): array
    {
        return $this->createQueryBuilder('e')
            ->select('e.id, e.categoryName', 'lim.limitsAmount', 'lim.limitsActive')
            ->leftJoin('App\Entity\Limits', 'lim', 'WITH', 'e.id = lim.limitsCategory')
            ->andWhere('e.id = :id')
            ->andWhere('lim.limitsDate = :date')
            ->setParameter('id', $id)
            ->setParameter('date', $date)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    
}
