<?php

namespace App\Repository;

use App\Entity\Expense;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Expense|null find($id, $lockMode = null, $lockVersion = null)
 * @method Expense|null findOneBy(array $criteria, array $orderBy = null)
 * @method Expense[]    findAll()
 * @method Expense[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExpenseRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Expense::class);
    }

    public function findSum(array $dates, int $catId = 0): ?float
    {
        $exps = $this->createQueryBuilder('i')
            ->select('SUM(i.expenseAmount) AS expsum')
            ->andwhere('i.expenseStamp BETWEEN :date1 AND :date2')
            ->setParameter('date1', $dates[0])
            ->setParameter('date2', $dates[1]);

        if ($catId > 0) {
            $exps->andWhere('i.expenseCategoryId = :catid')
                ->setParameter('catid', $catId);
        }

        $res = $exps->setMaxResults(1)->getQuery()->execute();
        return $res[0]['expsum'];
    }

}
